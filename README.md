# Watercolor Games Webserver

The Watercolor Games Webserver is a specialized, cross-platform web server written from scratch that acts as both a flatfile webserver and a REST API to a backend database.

It is built as the backend for [the Watercolor website](https://getshiftos.net/) and works on Linux (Ubuntu 16.04 with Mono) and Windows (.NET Framework 4.5).

### Setup

If you'd like to set up the Watercolor webserver on your own system either for contributing to the main project or adapting to your own use case, you'll need to do a few things, depending on whether you would like to use SSL or not.

#### Use without SSL

First, open the `Program.cs` file in `WatercolorGames.Website`. This is where the webserver is initialized. Change the line that says

```csharp
            var server = new ActualServer(8080);
```

to say:

```csharp
            var server = new ActualServer(80);
```

This will make the server listen on port 80. You will also need to go to the `HttpServer.cs` file in the same project and change the line that says:

```csharp
            listener = new TcpListener(IPAddress.Loopback, port);
```

so that it says:

```csharp
            listener = new TcpListener(IPAddress.Any, port);
```

This will make the server accept traffic from the outside world (provided your firewall and router configurations allow it) rather than only listening to traffic coming from your system.

You should just be able to compile and run the resulting `WatercolorGames.Website.exe` and have a functioning (insecure) webserver.

#### For use with SSL

Thankfully, no code changes, but you may need to set up an HTTP reverse proxy (NGINX is a good one).

Follow the documentation for your reverse proxy to set up SSL on port 443 and regular HTTP on 80. The reverse proxy should be forwarding all traffic on port 80 to port 443 via an HTTP 301 redirect (this is to ensure that the user is using HTTPS and not raw HTTP). The reverse proxy should then proxy all traffic on port 443 to `127.0.0.1:8080`, allowing the Watercolor server to take over from there.

In this case, if the server is working fine, you'll be able to visit your website from anywhere and get the Watercolor webserver frontend. If the reverse proxy is working fine and the Watercolor server is offline, you will get **502 Bad Gateway** errors. This just means your reverse proxy couldn't establish a connection to the destination.

### The `www` directory

It's worth noting that the **www** directory in the build output folder is WORLD-ACCESSIBLE. This is where the HTML, CSS, JavaScript and other files for the frontend are stored. Do NOT store personal data in there that you don't want shared with the world.

(And especially, do not store your SSL private key in there.)

Also, the `www` directory in the WatercolorGames.Website project is copied to the build output, so don't put anything personal in there either.