﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Cryptography;
using System.Text;

namespace WatercolorGames.Website.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private ActualServer server = null;

        [Dependency]
        private UserManager _usermanager = null;


        private void initialize()
        {
            server = new ActualServer(8080);
            server.Inject(this);
        }

        private void stop()
        {
            server = null;
        }

        [TestMethod]
        public void WeakPasswordTest()
        {
            initialize();
            string[] inputs = new string[]
            {
                "1234",
                "abcd",
                "abc123",
                "Abc123",
                "p4$$w0rd",
                "hUrrDuRR",
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                "ilovepussy",
                "thr1ll3r"
            };
            foreach(var password in inputs)
            {
                Assert.IsFalse(_usermanager.validate_pwd(password), $"Password {password} is not seen as weak by the API.");
            }
            stop();
        }

        private string generateRandomPassword(int lengthBytes)
        {
            using (var rnd = RandomNumberGenerator.Create())
            {
                byte[] bytes = new byte[lengthBytes];
                rnd.GetNonZeroBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }

        [TestMethod]
        public void RandomStrongPasswordTest()
        {
            initialize();
            for (int l = 12; l < 256; l++)
            {
                for (int i = 0; i < 10000; i++)
                {
                    string password = generateRandomPassword(l);
                    Assert.IsTrue(_usermanager.validate_pwd(password), $"Strong password {password} is weak to the API.");
                }
            }
            stop();
        }
    }
}
