﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WatercolorGames.Website
{
    public interface IServerComponent
    {
        void Initiate();
        void Unload();

        bool OnGET(UrlPath path, HttpProcessor processor);
        bool OnPOST(UrlPath path, HttpProcessor processor, StreamReader inputData);
    }

    public class DependencyAttribute : Attribute { }
}
