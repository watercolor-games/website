﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatercolorGames.Utilities;

namespace WatercolorGames.Website
{
    public class UserManager : IServerComponent
    {
        [Dependency]
        private DatabaseManager _database = null;
        private LiteDB.LiteCollection<User> _userdb = null;

        public void Initiate()
        {
            Logger.Log("User manager is starting...");
            _userdb = _database.Database.GetCollection<User>("wg_users");
            _userdb.EnsureIndex(x => x.Id, true);
            var admin = _userdb.FindOne(x => x.Permission == AccessControlLevel.Administrator);
            if(admin == null)
            {
                Logger.Log("No administrator account found in the database. Please create one.", LogType.Warning);
                Console.WriteLine("");
                Console.WriteLine("Administrator account not found. An administrator account allows you to control the Watercolor site from within your browser - managing users, permissions, and many other things. Needless to say, you need an admin account to use this thing. So, let's create one. Just enter an email address, username, and a password and we'll get you set up.");
                CreateAcct:
                Console.WriteLine();
                Console.Write("Username: ");
                string username = Console.ReadLine();
                Console.Write("Email address: ");
                string email = Console.ReadLine();
                Console.Write("Password: ");
                string pwd = ReadPassword();
                Console.Write("Password [confirm]: ");
                string cpwd = ReadPassword();
                if(pwd != cpwd)
                {
                    Console.WriteLine("Your passwords do not match.");
                    goto CreateAcct;
                }
                var result = CreateUser(email, username, pwd, AccessControlLevel.Administrator);
                if(result == RegisterResult.Success)
                {
                    Console.WriteLine("Admin account created successfully.");
                }
                else
                {
                    string res = "An unknown issue occurred.";
                    switch (result)
                    {
                        case RegisterResult.NoUserOrEmail:
                            res = "A blank username or email was provided. This is not supported.";
                            break;
                        case RegisterResult.UserOrEmailTaken:
                            res = "The provided username or email has already been taken.";
                            break;
                        case RegisterResult.WeakPassword:
                            res = "The provided password is too weak. Passwords must be at least 8 characters in length and contain at least one lowercase, UPPERCASE, number and symbol character.";
                            break;
                    }
                    Console.WriteLine("User API error: {0}", res);
                    goto CreateAcct;
                }
            }
            Logger.Log($"Ready. {_userdb.Count()} user(s) found.");
        }

        public static string ReadPassword(char mask)
        {
            const int ENTER = 13, BACKSP = 8, CTRLBACKSP = 127;
            int[] FILTERED = { 0, 27, 9, 10 /*, 32 space, if you care */ }; // const

            var pass = new Stack<char>();
            char chr = (char)0;

            while ((chr = System.Console.ReadKey(true).KeyChar) != ENTER)
            {
                if (chr == BACKSP)
                {
                    if (pass.Count > 0)
                    {
                        System.Console.Write("\b \b");
                        pass.Pop();
                    }
                }
                else if (chr == CTRLBACKSP)
                {
                    while (pass.Count > 0)
                    {
                        System.Console.Write("\b \b");
                        pass.Pop();
                    }
                }
                else if (FILTERED.Count(x => chr == x) > 0) { }
                else
                {
                    pass.Push((char)chr);
                    System.Console.Write(mask);
                }
            }

            System.Console.WriteLine();

            return new string(pass.Reverse().ToArray());
        }

        /// <summary>
        /// Like System.Console.ReadLine(), only with a mask.
        /// </summary>
        /// <returns>the string the user typed in </returns>
        public static string ReadPassword()
        {
            return ReadPassword('*');
        }

        public RegisterResult CreateUser(string email, string username, string password, AccessControlLevel perm = AccessControlLevel.User)
        {
            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(username))
                return RegisterResult.NoUserOrEmail;
            var existing = _userdb.FindOne(x => x.Username == username || x.Email == email);
            if (existing != null)
                return RegisterResult.UserOrEmailTaken;
            if (validate_pwd(password) == false)
                return RegisterResult.WeakPassword;
            byte[] salt = PasswordHasher.GenerateRandomSalt();
            string hash = PasswordHasher.Hash(password, salt);
            var user = new User
            {
                About = "",
                CurrentSessionToken = gen_token(),
                Email = email,
                EmailVerified = false,
                Id = Guid.NewGuid().ToString(),
                IsBanned = false,
                LastLogin = null,
                Username = username,
                PasswordHash = hash,
                PasswordSalt = Convert.ToBase64String(salt),
                Permission = perm,
                RealName = ""
            };
            _userdb.Insert(user);
            return RegisterResult.Success;
        }

        private bool validate_pwd(string password)
        {
            if (password == null)
                return false;
            bool hasNumber = false;
            bool hasMinimumLength = password.Length >= 8;
            foreach(char c in password)
            {
                if (char.IsDigit(c))
                    hasNumber = true;
            }
            return hasNumber && hasMinimumLength;
        }

        public User GetUserFromToken(string token)
        {
            return _userdb.FindOne(x => x.CurrentSessionToken == token);
        }

        public bool Login(string email, string password, out string token)
        {
            var user = _userdb.FindOne(x => x.Email == email);
            if(user == null)
            {
                token = null;
                return false;
            }
            string saltb64 = user.PasswordSalt;
            byte[] salt = Convert.FromBase64String(saltb64);
            string hash = PasswordHasher.Hash(password, salt);
            if(hash != user.PasswordHash)
            {
                token = null;
                return false;
            }
            //User's last login time is updated to now
            user.LastLogin = DateTime.UtcNow.ToString();
            _userdb.Update(user);
            token = user.CurrentSessionToken;
            return true;
        }

        public User GetUserFromID(string id)
        {
            return _userdb.FindOne(x => x.Id == id);
        }

        public string ResetUserToken(string token)
        {
            var user = _userdb.FindOne(x => x.CurrentSessionToken == token);
            if (user == null)
                return null;
            user.CurrentSessionToken = gen_token();
            _userdb.Update(user);
            return user.CurrentSessionToken;
        }

        public string gen_token()
        {
            return Convert.ToBase64String(PasswordHasher.GenerateRandomSalt());
        }

        public bool OnGET(UrlPath path, HttpProcessor processor)
        {
            return false;
        }

        public bool OnPOST(UrlPath path, HttpProcessor processor, StreamReader inputData)
        {
            return false;
        }

        public void Unload()
        {
        }
    }

    public class UsersAPIComponent : IAPIController
    {
        [Dependency]
        private UserManager _users = null;

        public class LoginRequest
        {
            public string email { get; set; }
            public string password { get; set; }
        }

        public class RegisterRequest
        {
            public string username { get; set; }
            public string email { get; set; }
            public string password { get; set; }
        }

        public string EndpointCategory
        {
            get
            {
                return "users";
            }
        }

        public bool HandleGET(string endpoint, string[] parameters, HttpProcessor processor)
        {
            var token = processor.httpHeaders["Authentication"] as string;
            var user = _users.GetUserFromToken(token);

            switch (endpoint)
            {
                case "getid":
                    if(user != null)
                    {
                        processor.writeSuccess("text/plain");
                        processor.outputStream.Write(user.Id);
                    }
                    else
                    {
                        processor.writeForbidden();
                    }
                    return true;
                case "validatesession":
                    processor.writeSuccess("application/json");
                    processor.outputStream.Write(JsonConvert.SerializeObject(new
                    {
                        tokenValid = user != null
                    }));

                    return true;
                default:
                    var userById = _users.GetUserFromID(endpoint);
                    if (userById == null)
                    {
                        processor.writeFailure();
                        processor.outputStream.WriteLine("No user with the specified id could be found.");
                        return false;
                    }
                    processor.writeSuccess("application/json");
                    processor.outputStream.Write(userById.ToSafeJSON());
                    return true;
            }
            return false;
        }

        public bool HandlePOST(string endpoint, string[] parameters, HttpProcessor processor, StreamReader input)
        {
            string req = input.ReadToEnd();
            switch (endpoint)
            {
                case "login":
                    var loginObj = JsonConvert.DeserializeObject<LoginRequest>(req);
                    string loginToken;
                    if(_users.Login(loginObj.email, loginObj.password, out loginToken))
                    {
                        processor.writeSuccess("text/plain");
                        processor.outputStream.Write(loginToken);
                        return true;
                    }
                    else
                    {
                        processor.writeForbidden();
                        return true;
                    }
                case "register":
                    var regRequest = JsonConvert.DeserializeObject<RegisterRequest>(req);
                    var regResult = _users.CreateUser(regRequest.email, regRequest.username, regRequest.password);
                    if(regResult == RegisterResult.Success)
                    {
                        processor.writeSuccess("text/plain");
                    }
                    else
                    {
                        processor.writeForbidden();
                        string res = "";
                        switch (regResult)
                        {
                            case RegisterResult.NoUserOrEmail:
                                res = "A blank username or email was provided. This is not supported.";
                                break;
                            case RegisterResult.UserOrEmailTaken:
                                res = "The provided username or email has already been taken.";
                                break;
                            case RegisterResult.WeakPassword:
                                res = "The provided password is too weak. Passwords must be at least 8 characters in length and contain at least one lowercase, UPPERCASE, number and symbol character.";
                                break;
                        }
                        processor.outputStream.Write(res);
                    }
                    return true;
            }
            return false;
        }

        public void Initialize()
        {
        }

        public void Unload()
        {
        }
    }

    public class User
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public bool EmailVerified { get; set; }
        public AccessControlLevel Permission { get; set; }
        public bool IsBanned { get; set; }
        public string BannedFor { get; set; }
        public string BannedBy { get; set; }
        public string RealName { get; set; }
        public string About { get; set; }
        public string CurrentSessionToken { get; set; }
        public string LastLogin { get; set; }

        public string ToSafeJSON()
        {
            return JsonConvert.SerializeObject(new
            {
                email = Email,
                username = Username,
                about = About,
                id = Id,
                verified = EmailVerified,
                permission = Permission.ToString(),
                banned = IsBanned,
                bannedBy = BannedBy,
                bannedReason = BannedFor,
                fullname = RealName,
                lastLogin = LastLogin
            });
        }
    }

    public enum AccessControlLevel
    {
        User = 0,
        Trusted = 1,
        Developer = 2,
        Moderator = 3,
        Administrator = 4
    }

    public enum RegisterResult
    {
        Success,
        UserOrEmailTaken,
        WeakPassword,
        NoUserOrEmail
    }
}
