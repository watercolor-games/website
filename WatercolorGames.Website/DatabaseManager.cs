﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WatercolorGames.Website
{
    public class DatabaseManager : IServerComponent
    {
        private LiteDatabase database = null;
        private string _dbPath = null;

        public void Initiate()
        {
            _dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "watercolor.db");
            Utilities.Logger.Log("Initializing database engine (LiteDB)...");
            database = new LiteDatabase(_dbPath);
            Utilities.Logger.Log("Database engine ready.");
        }

        public LiteDatabase Database
        {
            get
            {
                return database;
            }
        }

        public bool OnGET(UrlPath path, HttpProcessor processor)
        {
            return false;
        }

        public bool OnPOST(UrlPath path, HttpProcessor processor, StreamReader inputData)
        {
            return false;
        }

        public void Unload()
        {
            Utilities.Logger.Log("Commencing final database shutdown!");
            database.Dispose();
            Utilities.Logger.Log("Database is shut down, bye bye.");
        }
    }
}
