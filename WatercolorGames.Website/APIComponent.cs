﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WatercolorGames.Utilities;

namespace WatercolorGames.Website
{
    public class APIComponent : IServerComponent
    {
        [Dependency]
        private ActualServer _server = null;

        [Dependency]
        private DatabaseManager _database = null;

        private LiteCollection<Session> _sessions = null;

        private bool _running = true;

        private List<IAPIController> _controllers = new List<IAPIController>();

        public void Initiate()
        {
            foreach(var controller in ReflectMan.Types)
            {
                if (controller.GetInterfaces().Contains(typeof(IAPIController)))
                {
                    var api = (IAPIController)Activator.CreateInstance(controller, null);
                    _server.Inject(api);
                    _controllers.Add(api);
                    api.Initialize();
                }
            }
            _sessions = _database.Database.GetCollection<Session>("apiSessions");
            _sessions.EnsureIndex(x => x.Id);
            Task.Run(() =>
            {
                Utilities.Logger.Log("Session manager started.", LogType.Info, "sessions");
                while (_running)
                {
                    lock (_database)
                    {

                        var session = _sessions.FindOne(x => x.ExpiresOn <= DateTime.UtcNow);
                        if (session != null)
                        {
                            Utilities.Logger.Log($"API session {session.Id} has expired. Removing from database.", LogType.Warning, "sessions");
                            _sessions.Delete(x => x.Id == session.Id);
                        }
                    }
                    Thread.Sleep(30000);
                }
            });
        }

        public bool OnGET(UrlPath path, HttpProcessor processor)
        {
            lock (_database)
            {
                string session = processor.httpHeaders["Authorization"]?.ToString();

                string[] pathComponents = path.RealPath.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                if (pathComponents.Length < 3)
                    return false;
                if (pathComponents[0] != "api")
                    return false;
                string cat = pathComponents[1];
                string endpoint = pathComponents[2];
                if (cat == "sessions" && endpoint == "grant")
                {
                    GenID:
                    string newSessionId = Convert.ToBase64String(PasswordHasher.GenerateRandomSalt());
                    if (_sessions.FindOne(x => x.Id == newSessionId) != null)
                        goto GenID;
                    _sessions.Insert(new Session
                    {
                        Id = newSessionId,
                        ExpiresOn = DateTime.UtcNow.AddMinutes(5)
                    });
                    processor.writeSuccess("text/plain");
                    processor.outputStream.Write(newSessionId);
                    Utilities.Logger.Log("New session id granted.", LogType.Info, "sessions");
                    return true;
                }
                var existingSession = _sessions.FindOne(x => x.Id == session);
                if (existingSession == null)
                {
                    processor.writeForbidden();
                    return true;
                }
                if (cat == "sessions" && endpoint == "heyimstillhere")
                {
                    existingSession.ExpiresOn = DateTime.UtcNow.AddMinutes(5);
                    Utilities.Logger.Log($"Session id {session} granted 5 extra minutes", LogType.Warning, "sessions");
                    processor.writeSuccess();
                    return true;
                }
                string[] @params = new string[pathComponents.Length - 3];
                for (int i = 0; i < @params.Length; i++)
                {
                    @params[i] = pathComponents[i + 3];
                }
                var component = _controllers.FirstOrDefault(x => x.EndpointCategory == cat);
                if (component == null)
                    return false;
                return component.HandleGET(endpoint, @params, processor);
            }
        }

        public bool OnPOST(UrlPath path, HttpProcessor processor, StreamReader inputData)
        {
            lock (_database)
            {
                string session = processor.httpHeaders["Authorization"]?.ToString();
                var existingSession = _sessions.FindOne(x => x.Id == session);
                if (existingSession == null)
                {
                    processor.writeForbidden();
                    return true;
                }
                string[] pathComponents = path.RealPath.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                if (pathComponents.Length < 3)
                    return false;
                if (pathComponents[0] != "api")
                    return false;
                string cat = pathComponents[1];
                string endpoint = pathComponents[2];
                string[] @params = new string[pathComponents.Length - 3];
                for (int i = 0; i < @params.Length; i++)
                {
                    @params[i] = pathComponents[i + 3];
                }
                var component = _controllers.FirstOrDefault(x => x.EndpointCategory == cat);
                if (component == null)
                    return false;

                return component.HandlePOST(endpoint, @params, processor, inputData);
            }
        }

        public void Unload()
        {
            _running = false;
            while(_controllers.Count>0)
            {
                _controllers[0].Unload();
                _controllers.RemoveAt(0);
            }
        }
    }

    public interface IAPIController
    {
        void Initialize();
        string EndpointCategory { get; }
        bool HandleGET(string endpoint, string[] parameters, HttpProcessor processor);
        bool HandlePOST(string endpoint, string[] parameters, HttpProcessor processor, StreamReader input);
        void Unload();
    }

    public class Session
    {
        public string Id { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
