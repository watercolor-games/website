﻿let userid;
let userdata;
let pageMode;
let session;

function isLoggedIn() {
    let token = localStorage.getItem("token");
    if (token == "null" || token == null) {
        return false;
    }
    return true;
}

function getLocalToken() {
    if (isLoggedIn())
        return localStorage.getItem("token");
    return null;
}

function queryUserProfile() {
    $.ajax("/api/users/" + userid, {
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            userdata = result;
            resetView();
        }
    });
}

function queryUserID() {
    $.ajax("/api/users/getid", {
        type: "GET",
        contentType: "text/plain",
        success: function (result) {
            userid = result;
            queryUserProfile();
        }
    });
}

function onPageLoad() {
    $(".error-offline").hide();
    //Stolen from https://bugs.vicr123.com/app/app.js - makes any AJAX requests send in an authentication header if the client is logged in.
    $.ajaxSetup({
        beforeSend: function (xhr) {
            let token = localStorage.getItem("token");
            if (isLoggedIn()) {
                xhr.setRequestHeader("Authentication", token);
            }
            xhr.setRequestHeader("Authorization", session);            
        }
    });

    //Reset the view even if our session is invalid, just to make things go faster.
    resetView();

    //Request a session id from the server.
    requestSession(validateSession, goOffline);

    //Ping the server every two minutes to make sure our session id is good
    setTimeout(function () {
        if (session == null) {
            requestSession(validateSession, goOffline);
        }
        else {
            pingServer(resetView, goOffline);
        }
    }, 120000);
}

function pingServer(onComplete, onError) {
    $.ajax("/api/sessions/heyimstillhere", {
        type: "GET",
        contentType: "text/plain",
        success: function () {
            onComplete();
        },
        error: function () {
            onError();
        }
    })
}

function goOffline() {
    $("[class^=page-]").hide();
    $(".error-offline").show();
}

function requestSession(onComplete, onError) {
    $.ajax("/api/sessions/grant", {
        type: "GET",
        contentType: "text/plain",
        success: function (result) {
            session = result;
            onComplete();
        },
        error: function () {
            session = null;
            onError();
        }
    });
}

function validateSession() {
    $.ajax("/api/users/validatesession",
        {
            type: "GET",
            contentType: "application/json",
            success: function (result, status, xhr) {
                if (!result.tokenValid) {
                    if (isLoggedIn()) {
                        localStorage.removeItem("token");
                    }
                    resetView();
                }
                else {
                    queryUserID();
                }
            }
        });
}

function resetView() {
    pageMode = getParameterByName("page");
    if (pageMode == "null" || pageMode == null) {
        pageMode = "home";
    }
    $("[class^=page-]").hide();
    $(".page-" + pageMode).show();
    $("[class^=pagenav-]").removeClass("active");
    $(".pagenav-" + pageMode).addClass("active");
    if (isLoggedIn()) {
        $(".guest").hide();
        $(".sessiononly").show();
    }
    else {
        $(".guest").show();
        $(".sessiononly").hide();
    }
    $(".username").html(properUsername());
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function properUsername() {
    if (userdata !== null && userdata !== undefined) {
        if (isLoggedIn()) {
            if (userdata.fullname !== null) {
                return userdata.fullname + " (" + userdata.username + ")";
            }
            return userdata.username;
        }
    }
    return "Guest";
}

function logout() {
    if (isLoggedIn()) {
        localStorage.removeItem("token");
        resetView();
    }
}

function handleLoginForm() {

    var email = $("#login-email").val();
    var pwd = $("#login-password").val();
    $.ajax("/api/users/login", {
        success: function (result, status, xhr) {
            localStorage.setItem("token", result);
            queryUserID();
            $("#modal-login").modal("hide");

        },
        data: JSON.stringify({ email: email, password: pwd }),
        contentType: "application/json",
        type: "POST",
        error: function (xhr, status, error) {
            alert("Error logging in.");
        }
    });
    return false;
}

window.onload = onPageLoad;