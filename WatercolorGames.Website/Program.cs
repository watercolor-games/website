﻿using Ganss.XSS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WatercolorGames.Utilities;

namespace WatercolorGames.Website
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var server = new ActualServer(8080);
            //Start HTTP server
	    Task.Run(()=>{
	    	server.listen();
	    });
	    Console.ReadKey(false);
	    server.stop();
        }
    }

    public class ActualServer : HttpServer
    {
        private List<ComponentInfo> _components = new List<ComponentInfo>();

        private void RecursiveInit(IServerComponent component)
        {
            foreach (var field in component.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Where(f => f.GetCustomAttributes(false).Any(t => t is DependencyAttribute)))
            {
                if (field.FieldType == this.GetType())
                    continue;
                else
                {
                    var c = GetEngineComponent(field.FieldType);
                    RecursiveInit(c);
                }
            }
            if (_components.FirstOrDefault(x => x.Component == component).IsInitiated == false)
            {
                component.Initiate();
                _components.FirstOrDefault(x => x.Component == component).IsInitiated = true;
            }
        }

        public IServerComponent GetEngineComponent(Type t)
        {
            if (!typeof(IServerComponent).IsAssignableFrom(t) || t.GetConstructor(Type.EmptyTypes) == null)
                throw new ArgumentException($"{t.Name} is not an IEngineComponent, or does not provide a parameterless constructor.");
            return _components.First(x => t.IsAssignableFrom(x.Component.GetType())).Component;
        }

        internal object Inject(object client)
        {
            foreach (var field in client.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Where(f => f.GetCustomAttributes(false).Any(t => t is DependencyAttribute)))
            {
                if (field.FieldType == this.GetType())
                    field.SetValue(client, this);
                else
                    field.SetValue(client, GetEngineComponent(field.FieldType));
            }
            return client;
        }

        public string SanitizeHtml(string wirus)
        {
            var sanitizer = new HtmlSanitizer();
            sanitizer.AllowedAttributes.Clear();
            sanitizer.AllowedTags.Clear();
            return sanitizer.Sanitize(wirus);
        }


        public ActualServer(int port) : base(port)
        {
            if (!Directory.Exists("www"))
                Directory.CreateDirectory("www");
            _components.Clear();
            foreach (var type in ReflectMan.Types.Where(x => x.GetInterfaces().Contains(typeof(IServerComponent))))
            {
                if (type.GetConstructor(Type.EmptyTypes) == null)
                {
                    Logger.Log($"Found {type.Name}, but it doesn't have a parameterless constructor, so it's ignored.  Probably a mistake.", LogType.Warning, "moduleloader");
                    continue;
                }
                Logger.Log($"Found {type.Name}", LogType.Info, "moduleloader");
                var componentInfo = new ComponentInfo
                {
                    IsInitiated = false,
                    Component = (IServerComponent)Activator.CreateInstance(type, null)
                };
                _components.Add(componentInfo);
            }
            foreach (var component in _components)
            {
                Logger.Log($"{component.Component.GetType().Name}: Injecting dependencies...", LogType.Info, "moduleloader");
                Inject(component.Component);
            }
            //I know. This is redundant. I'm only doing this as a safety precaution, to prevent crashes with modules that try to access uninitiated modules as they're initiating.
            foreach (var component in _components)
            {
                RecursiveInit(component.Component);
            }

        }

        public UrlPath Parse(string path)
        {
            string file = "";
            bool inParams = false;
            Dictionary<string, string> _params = new Dictionary<string, string>();
            string key = "";
            bool parsingValue = false;
            string value = "";
            for(int i=  0; i < path.Length; i++)
            {
                char c = path[i];
                if (inParams)
                {
                    switch (c)
                    {
                        case '=':
                            if (!parsingValue)
                            {
                                parsingValue = true;
                                continue;
                            }
                            break;
                        case '&':
                            if (parsingValue)
                            {
                                parsingValue = false;
                                _params.Add(key, value);
                                key = "";
                                value = "";
                                continue;
                            }
                            break;
                        default:
                            if(parsingValue)
                            {
                                value += c;
                            }
                            else
                            {
                                key += c;
                            }
                            continue;
                    }
                }
                else
                {
                    switch (c)
                    {
                        case '?':
                            inParams = true;
                            break;
                        default:
                            file += c;
                            break;
                    }
                }
            }
            if (!string.IsNullOrEmpty(key))
            {
                _params.Add(key, value);
            }
            file = file.Replace("/..", "");
            return new UrlPath
            {
                RealPath = file,
                File = file.Replace('/', Path.DirectorySeparatorChar).Remove(0,1),
                Parameters = _params
            };
        }

        public override void handleGETRequest(HttpProcessor p)
        {
            var path = this.Parse(p.http_url);
            if (path.File.ToLower().EndsWith(".php"))
            {
                p.writeFailure();
                p.outputStream.WriteLine("<h1>PHP Hypertext Preprocessor is NOT SUPPORTED.</h1>");
                p.outputStream.WriteLine($"<p>JavaScript, HTML5, CSS3, and C# master race. Please stop requesting me for PHP files. <em>Please.</em></p>");

            }
            foreach (var component in _components)
            {
                if (component.Component.OnGET(path, p))
                    return;
            }
            string realPath = Path.Combine(Environment.CurrentDirectory, "www", path.File);
            if (Directory.Exists(realPath))
            {
                string index = Path.Combine(realPath, "index.html");
                if (File.Exists(index))
                {
                    string extension = new FileInfo(index).Extension;
                    p.writeBinary(MimeTypes.MimeTypeMap.GetMimeType(extension), File.ReadAllBytes(index));
                }
                else
                {
                    p.writeFailure();
                    p.outputStream.WriteLine("<h1>File not found.</h1>");
                    p.outputStream.WriteLine($"<p><strong>Requested file:</strong> {SanitizeHtml(index)}</p>");
                }
            }
            else if (File.Exists(realPath))
            {
                string extension = new FileInfo(realPath).Extension;
                p.writeBinary(MimeTypes.MimeTypeMap.GetMimeType(extension), File.ReadAllBytes(realPath));

            }
            else
            {
                p.writeFailure();
                p.outputStream.WriteLine("<h1>File not found.</h1>");
                p.outputStream.WriteLine($"<p><strong>Requested file:</strong> {SanitizeHtml(realPath)}</p>");

            }

        }

        public override void handlePOSTRequest(HttpProcessor p, StreamReader inputData)
        {
            var path = Parse(p.http_url);
            foreach (var component in _components)
            {
                if (component.Component.OnPOST(path, p, inputData))
                    return;
            }
            p.writeFailure();
            p.outputStream.WriteLine("The requested API endpoint could not be found.");

        }
    }

    public class UrlPath
    {
        public string RealPath { get; set; }
        public string File { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
    }

    public class ComponentInfo
    {
        public bool IsInitiated { get; set; }
        public IServerComponent Component { get; set; }
    }
}
